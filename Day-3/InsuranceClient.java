interface Insurance {

    double calculatePremium(String vehicleName, double insuredAmount, String model);

    String payPremium(double premiumAmount, String vehicleNumber, String model);

}

class BajajInsurance implements Insurance {

    private static long counter = 10001;

    String policyNumber = new String();


    public double calculatePremium(final String vehicleName, final double insuredAmount, final String model) {
        
        final double premium = 0.05 * insuredAmount;
        return premium;
    }

    public String payPremium(final double premiumAmount, final String vehicleNumber, final String model) {
        
        policyNumber = policyNumber.concat(vehicleNumber).concat(model);;
        return policyNumber;
    }
}

class TataAIG implements Insurance {

    private static long policyCounter = 1000;

    String policyNumber = new String();


    
    public double calculatePremium(final String vehicleName, final double insuredAmount, final String model) {
        double premium = 0;

        if(insuredAmount < 100000) {

            premium = 0.04 * insuredAmount;
        }
        else {
            premium = 0.06 * insuredAmount;
        }
        return premium;
    }

    public String payPremium(final double premiumAmount, final String vehicleNumber, final String model) {

        policyNumber = policyNumber.concat(vehicleNumber).concat(model);
        return policyNumber;
    }
}
public class InsuranceClient {
    
    public static void main(String[] args) {

        Insurance insurance1 = null;

        String ins = args[0];
        switch(ins) {
            case "b": BajajInsurance bj = new BajajInsurance();
                      insurance1 = bj;
                      break;

            case "t": TataAIG ta = new TataAIG();
                      insurance1 = ta;
                      break;

            default: System.out.println("Invalid");
                     break;
        }
        
        double premiumAmt = insurance1.calculatePremium("Activa", 100000, "BAC");

        System.out.println("\n-----Insurance Premium amount is: Rs. " + premiumAmt);

        System.out.println("\n------Policy Number is: " + insurance1.payPremium(premiumAmt, "MH142220", "KAHTY"));
    }
}
