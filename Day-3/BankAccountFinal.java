abstract class BankAccount {

	private static long accountNumberTracker = 10000;

	private String customerName;

	private double accountBalance;

	private long accountNumber;

    //private final int bankAccountID;

	public abstract double withdraw(double amount);
	public abstract boolean validateBalance(double amount);
	public abstract void grantLoan(double amount);

    public BankAccount(String customerName, double accountBalance)
    {
        this.customerName = customerName;
        this.accountBalance = accountBalance;
    }

	public final double getAccountBalance() {
		return this.accountBalance;
	}
	
	public final double deposit(double amount) {

		this.accountBalance += amount;
		return amount;
	}

    
}

final class CurrentAccount extends BankAccount {

	private String businessName;
	private String gstNumber;
	private static long accountNumberTracker = 10000;

	private String customerName;

	private double accountBalance;

	private long accountNumber;
    

    public CurrentAccount(String customerName, double accountBalance, String businessName, String gstNumber)
    {
            super(customerName,accountBalance);
            this.businessName = businessName;
            this.gstNumber = gstNumber;
    }


	public final boolean validateBalance(double amount) {
			if(this.accountBalance > (amount + 25000))
				return true;
			else
				return false;
	}

		public final double withdraw(double amount) {
			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
	}

	public final void grantLoan(double amount) {
			if(amount <= 2500000)
				System.out.println("You are eligible for Loan of Rs." + amount);
			else 
				System.out.println("You are not eligible for Loan above 5L. ");
	}
}

final class SavingAccount extends BankAccount {

		/*public SavingAccount(String customerName, double accountBalance) {
			this.accountNumber = ++accountNumberTracker;
			this.customerName = customerName;
			this.accountBalance = accountBalance;
		}*/

	private static long accountNumberTracker = 10000;

	private String customerName;

	private double accountBalance;

	private long accountNumber;

        public SavingAccount(String customerName, double accountBalance)
        {
            super(customerName,accountBalance);
        }


		public final boolean validateBalance(double amount) {
			if(this.accountBalance > (amount + 10000) && amount <= 10000)
				return true;
			else
				return false;
		}

		public final double withdraw(double amount) {

			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
		}

		public final void grantLoan(double amount) {
			if(amount <= 500000)
				System.out.println("You are eligible for Loan of Rs." + amount);
			else 
				System.out.println("You are not eligible for Loan above Rs. 5L. ");
		}

}


final class SalariedAccount extends BankAccount {

	/*public SalariedAccount(String customerName, double accountBalance) {
			this.accountNumber = ++accountNumberTracker;
			this.customerName = customerName;
			this.accountBalance = accountBalance;
	}*/
	private static long accountNumberTracker = 10000;

	private String customerName;

	private double accountBalance;

	private long accountNumber;

    public SalariedAccount(String customerName, double accountBalance)
    {
            super(customerName,accountBalance);
    }

	public final boolean validateBalance(double amount) {
			if(this.accountBalance >= amount && amount <= 15000)
				return true;
			else
				return false;
	}

	public final double withdraw(double amount) {
			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
	}

	public final void grantLoan(double amount) {
			if(amount <= 1000000)
				System.out.println("You are eligible for Loan. of Rs." + amount);
			else 
				System.out.println("You are not eligible for Loan above 5L. ");
		}
}

public class BankAccountFinal {

	public static void main(String[] args) {

		BankAccount doc = null;
		String ch = args[0];

		switch(ch) {
			case "salary" :
						doc = new SavingAccount("John", 10000);
						System.out.println("Before withdrawal : " + doc.getAccountBalance());
						doc.withdraw(2000);
						System.out.println("After withdrawal : " + doc.getAccountBalance());
						break;

			case "current" :
						doc = new CurrentAccount("Alice", 80000, "ICICI", "GSTIN6332");
						doc.grantLoan(500000);
						break;

			case "salaried" :
						doc = new SalariedAccount("Anam", 100000);
						doc.grantLoan(5000000);
						break;

			default :
						System.out.println("Invalid");
						break;

		}
	}
}

