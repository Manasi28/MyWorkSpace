    import java.util.Scanner;

    public class Maxmin

    {

        public static void main(String[] args) 

        {

            int number1 = 0, number2 = 0;

            Scanner s = new Scanner(System.in);

            System.out.print("Enter no. of elements:");

            number1 = s.nextInt();

            int a[] = new int[number1];

            System.out.println("Enter the elements for array:");

            for (int i = 0; i < number1; i++) 

            {

                a[i] = s.nextInt();

            }

            for (int i = 0; i < number1; i++) 

            {

                for (int j = i + 1; j < number1; j++) 

                {

                    if (a[i] > a[j]) 

                    {

                        number2 = a[i];

                        a[i] = a[j];

                        a[j] = number2;

                    }

                }

            }

            System.out.println("\nMinimum of the array is" + " " + a[0]);
            
            System.out.println("\nMaximum of the array is" + " " + a[number1-1]);


        }

    }