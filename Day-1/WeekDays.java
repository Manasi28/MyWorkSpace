import java.util.Scanner;

public class WeekDays
{
    public static void main(String args[])
    {
        System.out.println("\n Enter a day");
        Scanner sc = new Scanner(System.in);

        String d = sc.nextLine();

        switch (d) {
            case "SUNDAY":
                System.out.println(" Its a Weekend !!");
                break;
            case "SATURDAY":
                System.out.println(" Its a Weekend !!");
                break;

            default:
                System.out.println(" It's a Weekday !!");
                break;
        }
    }
}