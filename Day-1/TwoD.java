public class TwoD
{
    public static void main(String args[])
    {
        int r = 5, c = 5;
        int arr[][] = new int[r][c];

        int initialvalue = 10;

        for(int rowI = 0; rowI < r; rowI++)
        {
             for(int colI = 0; colI < c; colI++ )
            {
                 arr[rowI][colI] = initialvalue++;
            }
        } 


        for(int rowI = 0; rowI < r; rowI++)
        {
            for(int colI = 0; colI < c; colI++ )
            {
               System.out.print("\t" + arr[rowI][colI] + "\t");
            }
            System.out.println();
        }
    }
    
}