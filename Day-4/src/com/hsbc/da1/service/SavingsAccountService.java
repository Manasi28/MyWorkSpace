package com.hsbc.da1.service;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.model.*;


public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city, String state, int zipCode);

	public void deleteSavingsAccount(long accountNumber);

	public SavingsAccount[] fetchSavingsAccounts();

	public SavingsAccount fetchAccountByPIN(int pin);

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);

	public double withdraw(long accountId, double amount);

	public double deposit(long accountId, double amount);

	public double checkBalance(long accountId);

	public void transfer(long accountId, long toId, double amount);
}
