package com.hsbc.da1.client;

import com.hsbc.da1.controller.*;
import com.hsbc.da1.model.*;

public class SavingsAccountClient {

	public static void main(String[] args) {

		SavingsAccountController controller = new SavingsAccountController();

		SavingsAccount sa1 = controller.openSavingsAccount("Ajay", 70000);
		SavingsAccount sa2 = controller.openSavingsAccount("Vijay", 90000);
		SavingsAccount sa3 = controller.openSavingsAccount("Sagar", 100000);

	
		controller.transfer(sa1.getAccountNumber(), sa2.getAccountNumber(), 3000);
		
		controller.transfer(sa2.getAccountNumber(), sa3.getAccountNumber(), 1000);

		double newBalance = controller.deposit(sa1.getAccountNumber(), 56000);
		
		controller.transfer(sa1.getAccountNumber(), sa2.getAccountNumber(), 2200);
		
		controller.transfer(sa2.getAccountNumber(), sa3.getAccountNumber(), 9000);

	
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();

		for (SavingsAccount savingsAccount : savingsAccounts) {
			if (savingsAccount != null) {
				System.out.println("Account Id : " + savingsAccount.getAccountNumber());
				System.out.println("Account Name : " + savingsAccount.getCustomerName());
				System.out.println("Account Balance : " + savingsAccount.getAccountBalance());
			} else {
				break;
			}
		}
	}
}
