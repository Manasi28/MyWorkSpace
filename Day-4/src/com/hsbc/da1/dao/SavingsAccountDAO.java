package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	SavingsAccount[] fetchSavingsAccounts();
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);

}
