package com.hsbc.da1.threadtasks;

public class ThreadDemoClient extends Thread {
	
	public static void main(String[] args) {
		
		System.out.println("Main thread when program started " + Thread.currentThread().getName());
		
		ThreadDemoClient t1 = new ThreadDemoClient();
		t1.setName("Child_Thread");
		
		
		t1.start();
		try {
				
			t1.join();
			
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
		e.printStackTrace();
		}
		System.out.println("------Task Completed------");
		
	}

	
	
	
	
	@Override
	public void run() {
		System.out.println("==================Thread " + Thread.currentThread().getName() + " start ==================");
		for(int i = 0; i < 5; i++)
		{
			System.out.println("Name of thread " + Thread.currentThread().getName());
			try {
				Thread.sleep(3000);
			}catch(InterruptedException e){
				System.out.println("Thread interrupted");
				
			}
		}
		System.out.println("==================Thread " + Thread.currentThread().getName() + " terminates ==================");
			
		}
		
	}
