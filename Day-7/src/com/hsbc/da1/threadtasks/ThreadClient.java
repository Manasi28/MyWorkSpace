package com.hsbc.da1.threadtasks;

public class ThreadClient {
	
	public static void main(String[] args) {
		
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickr = new Flickr();
		Runnable picassa = new Picassa();
		
		Thread googlePhoto = new Thread(googlePhotos);
		googlePhoto.setName("Google_Photos");
		googlePhoto.setPriority(5);
		
		Thread flickrr = new Thread(flickr);
		flickrr.setName("Flickr");
		flickrr.setPriority(10);
		
		Thread picasa = new Thread(picassa);
		picasa.setName("Picassa");
		picasa.setPriority(1);
		
		googlePhoto.start();
		flickrr.start();
		picasa.start();
		
		try {
			googlePhoto.join();
			flickrr.join();
			picasa.join();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			System.out.println("---------Task Completed--------");
		}	
		
	}

}

class GooglePhotos implements Runnable
{
	@Override
	public void run() {
		
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
		
}


class Flickr implements Runnable
{
	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
		
}

class Picassa implements Runnable
{
	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
		
	
}
