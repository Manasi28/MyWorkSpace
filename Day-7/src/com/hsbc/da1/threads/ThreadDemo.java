package com.hsbc.da1.threads;

public class ThreadDemo {
	
	public static void main(String[] args) {
		Thread t = Thread.currentThread();
		System.out.println("Name of thread " + t.getName());
		
		for(int i = 0; i< 10; i++)
		{
			System.out.println("Name of thread " + t.getName());
			System.out.println("State " + t.getState().name());
			try {
				Thread.sleep(2000);
			}catch(InterruptedException e){
				System.out.println("Thread interrupted");
				
			}finally {
				System.out.println("State in finally block is " + t.getState().name());
			}
		}
	}

}
