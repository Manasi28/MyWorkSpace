package com.hsbc.da1.threads;

public class ChildThread extends Thread{
	
	public static void main(String[] args) {
		
		System.out.println("Main thread when program started " + Thread.currentThread().getName());
		
		ChildThread t1 = new ChildThread();
		t1.setName("Thread_1");
		
		ChildThread t2 = new ChildThread();
		t2.setName("Thread_2");
		
		t1.start();
		t2.start();
		
		for(int i = 0; i< 10; i++)
		{
			System.out.println("Name of thread " +Thread.currentThread().getName());
			try {
					Thread.currentThread().sleep(2000);
			}catch(InterruptedException e){
				System.out.println("Thread interrupted");
				
			}
		
	}
		System.out.println("Main Thread is terminated");

}
	@Override
	public void run() {
		System.out.println("==================Thread " + Thread.currentThread().getName() + " start ==================");
		for(int i = 0; i < 10; i++)
		{
			System.out.println("Name of thread " + Thread.currentThread().getName());
			try {
				Thread.sleep(3000);
			}catch(InterruptedException e){
				System.out.println("Thread interrupted");
				
			}
		}
		System.out.println("==================Thread " + Thread.currentThread().getName() + " terminates ==================");
			
		}
	}
