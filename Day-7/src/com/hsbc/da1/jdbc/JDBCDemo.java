package com.hsbc.da1.jdbc;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCDemo {

	public static void main(String[] args) {
		try 
			(Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb", "admin", "password");
			Statement stmt = connection.createStatement();)
			{
			//int count = stmt.executeUpdate("insert into items (name,price) values ('ipad',30000)");
			//count = stmt.executeUpdate("insert into items (name,price) values ('iphone',55000)");

			//System.out.println("Number of items inserted : " + count);
			ResultSet resultSet = stmt.executeQuery("select * from items");
			
			while(resultSet.next()) {
				System.out.printf("Name of the items %s and the price is %f %n",resultSet.getString(1),resultSet.getDouble(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
