package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class LinkedListBackedSavingAccount implements SavingsAccountDAO{

	private List<SavingsAccount> savingsAccountList = new LinkedList<>();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccountList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa: savingsAccountList) {
			if(sa.getAccountNumber() == accountNumber)
			{
				sa = savingsAccount;
			}
			
		}
		return savingsAccount;
	
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount sa: savingsAccountList)
		{
			if(sa.getAccountNumber() == accountNumber)
			{
				this.savingsAccountList.remove(sa);
			}
		}
		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccountList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount sa: savingsAccountList)
		{
			if(sa.getAccountNumber() == accountNumber)
			{
				return sa;
			}
		}
		return null;
	}


}
