package com.hsbc.da1.client;

import java.util.List;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountClient {
	
	public static void main(String[] args) {
		
		SavingsAccountController controller = new SavingsAccountController();
		double newAccBalance = 0;
		SavingsAccount ac;
		
		SavingsAccount bobSavingsAccount = controller.openSavingsAccount("Bob", 10000);
		SavingsAccount anamSavingsAccount = controller.openSavingsAccount("Anam", 39000);
		
		try {
			controller.transfer(5429, anamSavingsAccount.getAccountNumber(), 5000);
			
			newAccBalance = controller.deposit(1001, 20000);
			
			ac = controller.fetchSavingsAccountByAccountId(1578);	
			
		}
		catch(InsufficientBalanceException exception)
		{
			System.out.println(exception.getMessage());
		}
		catch(CustomerNotFoundException exception){
			System.out.println(exception.getMessage());
			
		}
		
		
		System.out.println("\n");
		System.out.println("Account Id "+ bobSavingsAccount.getAccountNumber());
		System.out.println("Account Id "+ anamSavingsAccount.getAccountNumber());
		
		
		//System.out.println("Account Balance: " + newAccBalance);
		
		List<SavingsAccount> savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
				System.out.println("Account Balance : "+ savingsAccount.getAccountBalance());
				
			}
		}
		
	}
}
