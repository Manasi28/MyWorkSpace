package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {
	
	public static void main(String args[])
	{
		ItemController controller = new ItemController();
		
		Item store1 = controller.createItem("Curtans", 3000);
		
		Item store2 = controller.createItem("Sofa", 10000);
		
		Item store3 = controller.createItem("Chair", 1000);
		
		System.out.println("Name: " + store3.getItemName() + " Price: " + store3.getItemPrice());
		
		/*Item id = controller.fetchItemById(store2.getItemId());
		System.out.println(id);*/
		
		Item[] items = controller.listAllItem();
		
		for (Item ic : items) {
			if (ic != null) {
				System.out.println("Item Name : " + ic.getItemName());
				System.out.println("Item Price : " + ic.getItemPrice());
			
			} 
		}
	}

}