package com.hsbc.da1.controller;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceImpl;

public class ItemController {
	
	ItemService service = new ItemServiceImpl();
	
	public Item createItem(String itemName, double itemPrice)
	{
		return this.service.createItem(itemName, itemPrice);
	}

	public void delete(long itemId)
	{
		this.service.delete(itemId);
	}
	
	public Item[] listAllItem()
	{
		return this.service.listAllItem();
	}
	
	public Item fetchItemById(long itemId)
	{
		return this.service.fetchItemById(itemId);
	}
}
