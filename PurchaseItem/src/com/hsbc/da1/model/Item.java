package com.hsbc.da1.model;

public class Item {
	
	private static int counter = 100;
	private long itemId;
	private String itemName;
	private double itemPrice;
	
	
	public Item(String itemName, double itemPrice)
	{
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.itemId = ++counter;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public double getItemPrice() {
		return itemPrice;
	}


	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}


	public long getItemId() {
		return itemId;
	}


	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	
}
