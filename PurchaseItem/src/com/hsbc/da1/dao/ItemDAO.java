package com.hsbc.da1.dao;

import com.hsbc.da1.model.*;

public interface ItemDAO {
	
	public Item createItem(Item item);
	
	public Item[] listAllItems();
	
	public Item fetchItemById(long itemId);
	
	public void deleteItem(long itemId);
	
	public Item updateItem(long itemId, Item item);

	

}
