package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public class ItemDAOImpl implements ItemDAO{
	
	private static Item[] itemStore = new Item[100];
	
	private static int count = 0;

	@Override
	public Item createItem(Item item) {
		// TODO Auto-generated method stub
		itemStore[++count] = item;
		return item;
	}

	@Override
	public Item[] listAllItems() {
		// TODO Auto-generated method stub
		return itemStore;
	}

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		for(int i = 0; i < itemStore.length; i++)
		{
			if(itemStore[i].getItemId() == itemId)
			{
				return itemStore[i];
			}
		}
		return null;
	}

	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		for(int i = 0; i < itemStore.length; i++)
		{
			if(itemStore[i].getItemId() == itemId)
			{
				itemStore[i] = null;
			}
		}
		
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		// TODO Auto-generated method stub
		for(int i = 0; i < itemStore.length; i++)
		{
			if(itemStore[i].getItemId() == itemId)
			{
				itemStore[i] = item;
				return itemStore[i];
			}
		}
		return null;
	}

	
}
