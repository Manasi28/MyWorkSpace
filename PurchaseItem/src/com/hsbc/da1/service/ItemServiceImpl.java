package com.hsbc.da1.service;

import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.dao.ItemDAOImpl;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService{
	
	ItemDAO dao = new ItemDAOImpl();

	@Override
	public Item createItem(String itemName, double itemPrice) {
		// TODO Auto-generated method stub
		Item item1 = new Item(itemName,itemPrice);
		Item created = this.dao.createItem(item1);
		
		return created;
	}

	@Override
	public void delete(long itemId) {
		// TODO Auto-generated method stub
		this.dao.deleteItem(itemId);
	}

	@Override
	public Item[] listAllItem() {
		// TODO Auto-generated method stub
		return this.dao.listAllItems();
	}

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		Item item = this.dao.fetchItemById(itemId);
		return item;
	}
	
	

}
