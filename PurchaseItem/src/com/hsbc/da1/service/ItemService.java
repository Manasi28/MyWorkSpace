package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;

public interface ItemService {
	
	public Item createItem(String itemName, double itemPrice);
	
	public void delete(long itemId);
	
	public Item[] listAllItem();
	
	public Item fetchItemById(long itemId);

}
