public class CallbyVR {

    public static void main(String[] args) {

        int arr1[] = new int[] { 93, 45, 46, 52 };

        int opr1 = 10;
        int opr2 = 20;
        int opr3 = 30;

        System.out.println("----------Call by value\n-------------");
         System.out.printf("\nChanges before the call   %d %d  %d\n", opr1, opr2, opr3);
         callbyValue(opr1, opr2, opr3);
         System.out.printf("Data after the call:   %d %d %d\n", opr1, opr2, opr3);
         

        System.out.printf("\n-----Call by Reference, Changes before the call--------\n");
        for (int i : arr1) {
            System.out.println(i);
        }

        callbyRef(arr1);

        System.out.printf("\n-----Changes before the call-----\n");
        for (int i : arr1) {
            System.out.println(i);
        }
    }

    private static void callbyValue(int opr1, int opr2, int opr3) {
        opr1 = opr1 * 36;
        opr2 = opr2 * 36;
        opr3 = opr3 * 36;

        System.out.printf("Temperory changes:   %d %d %d\n", opr1, opr2, opr3);

    }

    private static void callbyRef(int[] arry) {
    
        System.out.print("---------Inside the method---------\n");

        arry[0] = 53;
        arry[1] = 75;
        arry[2] = 96;
        arry[3] = 87;
        for (int i : arry) {
            System.out.println(i);
        }

    }
}
