package com.hsbc.da1.service;

import java.util.Collection;
import java.util.List;

import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.util.SavingsAccountDAOFactory;

public class SavingsAccountServiceImpl implements SavingsAccountService {
	

	private SavingsAccountDAO dao;
	
	public SavingsAccountServiceImpl(SavingsAccountDAO dao) {
		this.dao = dao;
	}
	
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {
		
		//no validations
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}
	
	
	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		
		return this.dao.fetchSavingsAccounts();
	}
	
	public SavingsAccount fetchAccountByPIN(int pin) {
		return null;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount)throws CustomerNotFoundException,InsufficientBalanceException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if ( currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			}
			else
			{
				throw new InsufficientBalanceException("Balance Insufficient");
			}
		}
		return 0;
	}
	
	public double deposit(long accountId, double amount) throws CustomerNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public double checkBalance(long accountId)throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public void transfer(long accountId, long toId, double amount)throws CustomerNotFoundException,InsufficientBalanceException {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByAccountId(toId);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if ( updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}


}
