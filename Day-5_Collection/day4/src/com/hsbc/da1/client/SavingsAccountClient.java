package com.hsbc.da1.client;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException, CustomerNotFoundException {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your option");
		System.out.println("1) Array Backed");
		System.out.println("2) List Backed");
		System.out.println("3) Set Backed");
		
		System.out.println("-------------------------------------------------------------");
		
		int option = scanner.nextInt();
		
		System.out.println(" You entered option number :  "+ option);
		
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(option);
		
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);

		
		SavingsAccountController controller = new SavingsAccountController(service);
		
		//double newAccBalance = 0;
		//SavingsAccount ac;
		
		SavingsAccount bobSavingsAccount = controller.openSavingsAccount("Bob", 10000);
		SavingsAccount anamSavingsAccount = controller.openSavingsAccount("Anam", 39000);
		
		try {
			controller.transfer(bobSavingsAccount.getAccountNumber(), anamSavingsAccount.getAccountNumber(), 5000);
			
		}
		catch(InsufficientBalanceException exception)
		{
			System.out.println(exception.getMessage());
		}
		catch(CustomerNotFoundException exception){
			System.out.println(exception.getMessage());
			
		}
		
		
		System.out.println("\n");
		System.out.println("Account Id "+ bobSavingsAccount.getAccountNumber());
		System.out.println("Account Name "+ bobSavingsAccount.getCustomerName());
		System.out.println("Account Balance "+ bobSavingsAccount.getAccountBalance());
		
		System.out.println("\n");
		System.out.println("Account Id "+ anamSavingsAccount.getAccountNumber());
		System.out.println("Account Name "+ anamSavingsAccount.getCustomerName());
		System.out.println("Account Balance "+ anamSavingsAccount.getAccountBalance());
		
		
		//System.out.println("Account Balance: " + newAccBalance);
		
		/*Collection<SavingsAccount> savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
				System.out.println("Account Balance : "+ savingsAccount.getAccountBalance());
				
			}
		}*/
		
	}
}
