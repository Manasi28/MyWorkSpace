package com.hsbc.da1.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class HashSetBackedSavingAccount implements SavingsAccountDAO {
	
	private Set<SavingsAccount> set = new HashSet<>();
	
	Iterator<SavingsAccount> itr = set.iterator();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		set.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		while(itr.hasNext()) {
			SavingsAccount sa =itr.next();
			if(sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}

		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount sa : set) {
			if(sa.getAccountNumber() == accountNumber) {
				set.remove(sa);
			}
		}

		
	}

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return set;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		while(itr.hasNext()) {
			
			SavingsAccount savingsAccount=itr.next();
			if(savingsAccount.getAccountNumber() == accountNumber) {
			return savingsAccount;
			}
		}
			throw new CustomerNotFoundException("Customer not found") ;
	}
	
	

}
