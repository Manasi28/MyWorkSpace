package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {
	
	public static void main(String[] args) {
		
		SavingsAccount sa1 = new SavingsAccount("Bob",56000);
		
		SavingsAccount sa2 = new SavingsAccount("Anam",46000);
		
		SavingsAccount sa3 = new SavingsAccount("Joey",70000);
		
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		savingsAccountSet.add(sa1);
		savingsAccountSet.add(sa2);
		savingsAccountSet.add(sa3);
		
		System.out.println("The size of set is " + savingsAccountSet.size());
		Iterator<SavingsAccount> it = savingsAccountSet.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}

}
