package com.hsbc.da1.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	
	public static void main(String[] args) {
		Set<Integer> set = new HashSet<>();
		set.add(45);
		set.add(77);
		set.add(77);
		set.add(93);
		set.add(20);
		set.add(20);
		
		System.out.printf("Total number of elements %d %n", set.size());
		System.out.println(set.contains(45));
		
		Iterator<Integer> it = set.iterator();
		
		while(it.hasNext()) {
			int value = it.next();
			System.out.println("The value is " + value);
		}
		System.out.println("Size of the set : " + set.size());
		
	}

}
